﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_add = new System.Windows.Forms.Button();
            this.button_divide = new System.Windows.Forms.Button();
            this.button_multiply = new System.Windows.Forms.Button();
            this.button_subtract = new System.Windows.Forms.Button();
            this.button_log = new System.Windows.Forms.Button();
            this.button_sin = new System.Windows.Forms.Button();
            this.button_tan = new System.Windows.Forms.Button();
            this.button_cos = new System.Windows.Forms.Button();
            this.button_sqrt = new System.Windows.Forms.Button();
            this.label_display = new System.Windows.Forms.Label();
            this.button_clearAll = new System.Windows.Forms.Button();
            this.button_clear = new System.Windows.Forms.Button();
            this.textBox_result = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button_dr = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button_add
            // 
            this.button_add.Location = new System.Drawing.Point(271, 81);
            this.button_add.Name = "button_add";
            this.button_add.Size = new System.Drawing.Size(41, 39);
            this.button_add.TabIndex = 0;
            this.button_add.Text = "+";
            this.button_add.UseVisualStyleBackColor = true;
            this.button_add.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_divide
            // 
            this.button_divide.Location = new System.Drawing.Point(271, 128);
            this.button_divide.Name = "button_divide";
            this.button_divide.Size = new System.Drawing.Size(41, 39);
            this.button_divide.TabIndex = 1;
            this.button_divide.Text = "/";
            this.button_divide.UseVisualStyleBackColor = true;
            this.button_divide.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_multiply
            // 
            this.button_multiply.Location = new System.Drawing.Point(318, 128);
            this.button_multiply.Name = "button_multiply";
            this.button_multiply.Size = new System.Drawing.Size(46, 39);
            this.button_multiply.TabIndex = 2;
            this.button_multiply.Text = "*";
            this.button_multiply.UseVisualStyleBackColor = true;
            this.button_multiply.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_subtract
            // 
            this.button_subtract.Location = new System.Drawing.Point(318, 81);
            this.button_subtract.Name = "button_subtract";
            this.button_subtract.Size = new System.Drawing.Size(46, 39);
            this.button_subtract.TabIndex = 3;
            this.button_subtract.Text = "-";
            this.button_subtract.UseVisualStyleBackColor = true;
            this.button_subtract.Click += new System.EventHandler(this.button_add_Click);
            // 
            // button_log
            // 
            this.button_log.Location = new System.Drawing.Point(190, 209);
            this.button_log.Name = "button_log";
            this.button_log.Size = new System.Drawing.Size(75, 33);
            this.button_log.TabIndex = 4;
            this.button_log.Text = "log";
            this.button_log.UseVisualStyleBackColor = true;
            this.button_log.Click += new System.EventHandler(this.button_log_Click);
            // 
            // button_sin
            // 
            this.button_sin.Location = new System.Drawing.Point(354, 180);
            this.button_sin.Name = "button_sin";
            this.button_sin.Size = new System.Drawing.Size(75, 23);
            this.button_sin.TabIndex = 5;
            this.button_sin.Text = "sin";
            this.button_sin.UseVisualStyleBackColor = true;
            this.button_sin.Click += new System.EventHandler(this.button_sin_Click);
            // 
            // button_tan
            // 
            this.button_tan.Location = new System.Drawing.Point(190, 180);
            this.button_tan.Name = "button_tan";
            this.button_tan.Size = new System.Drawing.Size(75, 23);
            this.button_tan.TabIndex = 6;
            this.button_tan.Text = "tan";
            this.button_tan.UseVisualStyleBackColor = true;
            this.button_tan.Click += new System.EventHandler(this.button_tan_Click);
            // 
            // button_cos
            // 
            this.button_cos.Location = new System.Drawing.Point(271, 180);
            this.button_cos.Name = "button_cos";
            this.button_cos.Size = new System.Drawing.Size(75, 23);
            this.button_cos.TabIndex = 7;
            this.button_cos.Text = "cos";
            this.button_cos.UseVisualStyleBackColor = true;
            this.button_cos.Click += new System.EventHandler(this.button_cos_Click);
            // 
            // button_sqrt
            // 
            this.button_sqrt.Location = new System.Drawing.Point(271, 210);
            this.button_sqrt.Name = "button_sqrt";
            this.button_sqrt.Size = new System.Drawing.Size(75, 32);
            this.button_sqrt.TabIndex = 8;
            this.button_sqrt.Text = "sqrt";
            this.button_sqrt.UseVisualStyleBackColor = true;
            this.button_sqrt.Click += new System.EventHandler(this.button_sqrt_Click);
            // 
            // label_display
            // 
            this.label_display.AutoSize = true;
            this.label_display.Location = new System.Drawing.Point(351, 23);
            this.label_display.Name = "label_display";
            this.label_display.Size = new System.Drawing.Size(0, 17);
            this.label_display.TabIndex = 9;
            // 
            // button_clearAll
            // 
            this.button_clearAll.Location = new System.Drawing.Point(190, 77);
            this.button_clearAll.Name = "button_clearAll";
            this.button_clearAll.Size = new System.Drawing.Size(75, 42);
            this.button_clearAll.TabIndex = 10;
            this.button_clearAll.Text = "CE";
            this.button_clearAll.UseVisualStyleBackColor = true;
            this.button_clearAll.Click += new System.EventHandler(this.button_clearAll_Click);
            // 
            // button_clear
            // 
            this.button_clear.Location = new System.Drawing.Point(190, 127);
            this.button_clear.Name = "button_clear";
            this.button_clear.Size = new System.Drawing.Size(75, 40);
            this.button_clear.TabIndex = 11;
            this.button_clear.Text = "C";
            this.button_clear.UseVisualStyleBackColor = true;
            this.button_clear.Click += new System.EventHandler(this.button_clear_Click);
            // 
            // textBox_result
            // 
            this.textBox_result.Location = new System.Drawing.Point(12, 49);
            this.textBox_result.Name = "textBox_result";
            this.textBox_result.Size = new System.Drawing.Size(352, 22);
            this.textBox_result.TabIndex = 12;
            this.textBox_result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 159);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(53, 44);
            this.button1.TabIndex = 13;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(71, 159);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(56, 44);
            this.button2.TabIndex = 14;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(133, 159);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(51, 44);
            this.button3.TabIndex = 15;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(12, 118);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(53, 40);
            this.button4.TabIndex = 16;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button1_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(71, 118);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(56, 40);
            this.button5.TabIndex = 17;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button1_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(133, 118);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(51, 40);
            this.button6.TabIndex = 18;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button1_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(12, 77);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(53, 42);
            this.button7.TabIndex = 19;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button1_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(71, 77);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(56, 42);
            this.button8.TabIndex = 20;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button1_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(133, 77);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(51, 42);
            this.button9.TabIndex = 21;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button1_Click);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(12, 209);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(75, 31);
            this.button0.TabIndex = 22;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_dr
            // 
            this.button_dr.Location = new System.Drawing.Point(371, 245);
            this.button_dr.Name = "button_dr";
            this.button_dr.Size = new System.Drawing.Size(104, 42);
            this.button_dr.TabIndex = 23;
            this.button_dr.Text = "=";
            this.button_dr.UseVisualStyleBackColor = true;
            this.button_dr.Click += new System.EventHandler(this.button_dr_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 334);
            this.Controls.Add(this.button_dr);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox_result);
            this.Controls.Add(this.button_clear);
            this.Controls.Add(this.button_clearAll);
            this.Controls.Add(this.label_display);
            this.Controls.Add(this.button_sqrt);
            this.Controls.Add(this.button_cos);
            this.Controls.Add(this.button_tan);
            this.Controls.Add(this.button_sin);
            this.Controls.Add(this.button_log);
            this.Controls.Add(this.button_subtract);
            this.Controls.Add(this.button_multiply);
            this.Controls.Add(this.button_divide);
            this.Controls.Add(this.button_add);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_add;
        private System.Windows.Forms.Button button_divide;
        private System.Windows.Forms.Button button_multiply;
        private System.Windows.Forms.Button button_subtract;
        private System.Windows.Forms.Button button_log;
        private System.Windows.Forms.Button button_sin;
        private System.Windows.Forms.Button button_tan;
        private System.Windows.Forms.Button button_cos;
        private System.Windows.Forms.Button button_sqrt;
        private System.Windows.Forms.Label label_display;
        private System.Windows.Forms.Button button_clearAll;
        private System.Windows.Forms.Button button_clear;
        private System.Windows.Forms.TextBox textBox_result;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button_dr;
    }
}

