﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        double result=0;
        String operation;
        public Form1()
        {
            InitializeComponent();
        }

        private void button_add_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            operation = button.Text;
            Double.TryParse(textBox_result.Text, out result);
            label_display.Text = label_display.Text + button.Text;
            textBox_result.Text = "";
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            textBox_result.Text = textBox_result.Text + button.Text;
            label_display.Text = label_display.Text + button.Text;
        }

        private void button_clearAll_Click(object sender, EventArgs e)
        {
            textBox_result.Text = "";
            label_display.Text = "";
            result = 0;
        }

        private void button_clear_Click(object sender, EventArgs e)
        {
            textBox_result.Text = "";
            label_display.Text = "";
        }

        private void button_dr_Click(object sender, EventArgs e)
        {
            switch(operation)
            {
                case "+":
                    textBox_result.Text = (result + Double.Parse(textBox_result.Text)).ToString();
                    label_display.Text = "";
                    break;
                case "-":
                    textBox_result.Text = (result - Double.Parse(textBox_result.Text)).ToString();
                    label_display.Text = "";
                    break;
                case "*":
                    textBox_result.Text = (result * Double.Parse(textBox_result.Text)).ToString();
                    label_display.Text = "";
                    break;
                case "/":
                    textBox_result.Text = (result / Double.Parse(textBox_result.Text)).ToString();
                    label_display.Text = "";
                    break;
                default:
                    label_display.Text = "";
                    break;

            }
        }

        private void button_tan_Click(object sender, EventArgs e)
        {
            textBox_result.Text = (Math.Tan(Double.Parse(textBox_result.Text))).ToString();
        }

        private void button_cos_Click(object sender, EventArgs e)
        {
            textBox_result.Text = (Math.Cos(Double.Parse(textBox_result.Text))).ToString();
        }

        private void button_sin_Click(object sender, EventArgs e)
        {
            textBox_result.Text = (Math.Sin(Double.Parse(textBox_result.Text))).ToString();
        }

        private void button_log_Click(object sender, EventArgs e)
        {
            textBox_result.Text = (Math.Log(Double.Parse(textBox_result.Text))).ToString();
        }

        private void button_sqrt_Click(object sender, EventArgs e)
        {
            textBox_result.Text = (Math.Sqrt(Double.Parse(textBox_result.Text))).ToString();
        }
    }
}
