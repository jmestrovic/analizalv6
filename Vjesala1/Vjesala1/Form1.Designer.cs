﻿namespace Vjesala1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_inputWord = new System.Windows.Forms.TextBox();
            this.button_input = new System.Windows.Forms.Button();
            this.button_startPlay = new System.Windows.Forms.Button();
            this.button_check = new System.Windows.Forms.Button();
            this.label_checkDisplay = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_inputLetter = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label_tryL = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label_wordDisplay = new System.Windows.Forms.Label();
            this.button_exit = new System.Windows.Forms.Button();
            this.label_result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(51, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Unos rijeci:";
            // 
            // textBox_inputWord
            // 
            this.textBox_inputWord.Location = new System.Drawing.Point(43, 82);
            this.textBox_inputWord.Name = "textBox_inputWord";
            this.textBox_inputWord.Size = new System.Drawing.Size(100, 22);
            this.textBox_inputWord.TabIndex = 1;
            // 
            // button_input
            // 
            this.button_input.Location = new System.Drawing.Point(54, 110);
            this.button_input.Name = "button_input";
            this.button_input.Size = new System.Drawing.Size(75, 32);
            this.button_input.TabIndex = 2;
            this.button_input.Text = "Unesi";
            this.button_input.UseVisualStyleBackColor = true;
            this.button_input.Click += new System.EventHandler(this.button_input_Click);
            // 
            // button_startPlay
            // 
            this.button_startPlay.Location = new System.Drawing.Point(43, 226);
            this.button_startPlay.Name = "button_startPlay";
            this.button_startPlay.Size = new System.Drawing.Size(112, 78);
            this.button_startPlay.TabIndex = 3;
            this.button_startPlay.Text = "Igraj";
            this.button_startPlay.UseVisualStyleBackColor = true;
            this.button_startPlay.Click += new System.EventHandler(this.button_startPlay_Click);
            // 
            // button_check
            // 
            this.button_check.Location = new System.Drawing.Point(341, 110);
            this.button_check.Name = "button_check";
            this.button_check.Size = new System.Drawing.Size(81, 33);
            this.button_check.TabIndex = 4;
            this.button_check.Text = "Provjera";
            this.button_check.UseVisualStyleBackColor = true;
            this.button_check.Click += new System.EventHandler(this.button_check_Click);
            // 
            // label_checkDisplay
            // 
            this.label_checkDisplay.AutoSize = true;
            this.label_checkDisplay.Location = new System.Drawing.Point(2, 307);
            this.label_checkDisplay.Name = "label_checkDisplay";
            this.label_checkDisplay.Size = new System.Drawing.Size(221, 17);
            this.label_checkDisplay.TabIndex = 5;
            this.label_checkDisplay.Text = "Pritisnite \"Igraj\" da zapocnete igru";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(338, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Unesite slovo:";
            // 
            // textBox_inputLetter
            // 
            this.textBox_inputLetter.Location = new System.Drawing.Point(335, 82);
            this.textBox_inputLetter.Name = "textBox_inputLetter";
            this.textBox_inputLetter.Size = new System.Drawing.Size(100, 22);
            this.textBox_inputLetter.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(587, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Preostalih pokusaja:";
            // 
            // label_tryL
            // 
            this.label_tryL.AutoSize = true;
            this.label_tryL.Location = new System.Drawing.Point(644, 104);
            this.label_tryL.Name = "label_tryL";
            this.label_tryL.Size = new System.Drawing.Size(16, 17);
            this.label_tryL.TabIndex = 9;
            this.label_tryL.Text = "5";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(491, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 17);
            this.label5.TabIndex = 10;
            this.label5.Text = "Rijec:";
            // 
            // label_wordDisplay
            // 
            this.label_wordDisplay.AutoSize = true;
            this.label_wordDisplay.Location = new System.Drawing.Point(491, 202);
            this.label_wordDisplay.Name = "label_wordDisplay";
            this.label_wordDisplay.Size = new System.Drawing.Size(0, 17);
            this.label_wordDisplay.TabIndex = 11;
            // 
            // button_exit
            // 
            this.button_exit.Location = new System.Drawing.Point(670, 353);
            this.button_exit.Name = "button_exit";
            this.button_exit.Size = new System.Drawing.Size(90, 43);
            this.button_exit.TabIndex = 12;
            this.button_exit.Text = "Izlaz";
            this.button_exit.UseVisualStyleBackColor = true;
            this.button_exit.Click += new System.EventHandler(this.button_exit_Click);
            // 
            // label_result
            // 
            this.label_result.AutoSize = true;
            this.label_result.Location = new System.Drawing.Point(479, 307);
            this.label_result.Name = "label_result";
            this.label_result.Size = new System.Drawing.Size(46, 17);
            this.label_result.TabIndex = 13;
            this.label_result.Text = "label4";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label_result);
            this.Controls.Add(this.button_exit);
            this.Controls.Add(this.label_wordDisplay);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label_tryL);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox_inputLetter);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label_checkDisplay);
            this.Controls.Add(this.button_check);
            this.Controls.Add(this.button_startPlay);
            this.Controls.Add(this.button_input);
            this.Controls.Add(this.textBox_inputWord);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Vjesala";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_inputWord;
        private System.Windows.Forms.Button button_input;
        private System.Windows.Forms.Button button_startPlay;
        private System.Windows.Forms.Button button_check;
        private System.Windows.Forms.Label label_checkDisplay;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_inputLetter;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_tryL;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_wordDisplay;
        private System.Windows.Forms.Button button_exit;
        private System.Windows.Forms.Label label_result;
    }
}

