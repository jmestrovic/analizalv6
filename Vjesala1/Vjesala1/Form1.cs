﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vjesala1
{
    public partial class Form1 : Form
    {
        string text1;
        string path = "C:\\Users\\Jurica\\Downloads\\lista1.txt";
        int wordLength;
        List<char> guess = new List<char>();
        public Form1()
        {
            InitializeComponent();
        }

        private void button_input_Click(object sender, EventArgs e)
        {
            if (textBox_inputWord.Text == "")
            {
                MessageBox.Show("Krivi unos");
            }
            else
            {
                using (System.IO.StreamWriter write = new System.IO.StreamWriter(@path, true))
                {
                    write.WriteLine(textBox_inputWord.Text);
                }
                textBox_inputWord.Text = "";
            }
        }

        private void button_exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_startPlay_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int lines = 0;
            string[] nmb_lines = System.IO.File.ReadAllLines(@path);
            foreach (string line in nmb_lines)
            {
                lines++;
            }
            int rnd_line = rnd.Next(1, lines);
            text1 = System.IO.File.ReadLines(@path).Skip(rnd_line).Take(1).First();
            wordLength = text1.Length;
            label_checkDisplay.Text = "Igra zapoceta";
        }

        private void button_check_Click(object sender, EventArgs e)
        {
            if (label_tryL.Text != "0")
            {
                if (!text1.Contains(textBox_inputLetter.Text))
                {
                    label_tryL.Text = (Convert.ToInt32(label_tryL.Text) - 1).ToString();
                }
                else
                {
                    for (int i = 0; i < wordLength; i++)
                    {
                        if (text1.Contains(textBox_inputLetter.Text.ElementAt(i))) { label_wordDisplay.Text = label_wordDisplay.Text + textBox_inputLetter.Text.ElementAt(i); }
                        else { label_wordDisplay.Text = label_wordDisplay.Text + " _"; }
                    }
                }
            }
            else { MessageBox.Show("Izgubili ste"); }
        }
    }
}
